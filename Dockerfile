# Imagen Base
FROM node:latest

#Directorio de la APP en el contenedor
WORKDIR /app

# Copiado de archivos

ADD build/default /app/build/default
ADD server.js /app
ADD package.json /app


# Dependencias
RUN npm install

#Puerto que expongo en el contenedor
EXPOSE 3000

# Comando para ejecutar el servicio
CMD ["npm", "start"]
